﻿using AutoMapper.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using WepApicore2.Model;
using WepApicore2.DTOs;
using Task.DTOs;

namespace WepApicore2.Configuration
{
    public class MapperConfig:Profile
    {
        public static IMapper mapper { get; set; }
        static MapperConfig()
        {
            var config = new MapperConfiguration(
          cfg =>
          {
              cfg.CreateMap<ApplicationUser, ApplicationUserDTO>().ReverseMap();
              cfg.CreateMap<ApplicationUser, LoginDto>().ReverseMap();
          });

            mapper = config.CreateMapper();
        }
    }
}
