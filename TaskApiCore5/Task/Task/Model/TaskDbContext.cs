﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WepApicore2.Model
{
    public class TaskDbContext : IdentityDbContext
    {
        public TaskDbContext(DbContextOptions dbContext) : base(dbContext)
        {

        }
        public TaskDbContext() { }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
