﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WepApicore2.Model
{
    public class ApplicationUser:IdentityUser
    {
        [Required]
        public override string UserName { get; set; }
        [DataType(DataType.EmailAddress)]
        public override string Email { get; set; }
        [Required ,DataType(DataType.Password)]
        public string Password { get; set; }
        [Compare("Password"), DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }
    }
}
