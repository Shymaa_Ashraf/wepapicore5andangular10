﻿using AutoMapper;
using AutoMapper.Configuration;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Task.DTOs;
using WEP_APICore.HelpClasses;
using WepApicore2.Configuration;
using WepApicore2.DTOs;
using WepApicore2.Model;
using IConfiguration = Microsoft.Extensions.Configuration.IConfiguration;

namespace Task.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        protected readonly IMapper Mapper = MapperConfig.mapper;
        protected readonly UserManager<IdentityUser> _userManager;
        private readonly IConfiguration _configuration;
        private readonly TaskDbContext _taskDbContext;
        public AuthenticationController
            (
            UserManager<IdentityUser> userManager
            , IConfiguration configuration
            , TaskDbContext taskDbContext
            )
        {
            _userManager = userManager;
            _configuration = configuration;
            _taskDbContext = taskDbContext;
        }
        [HttpPost("Register")]
        public async Task<IActionResult> Register(ApplicationUserDTO userDto)
        {
            var user = Mapper.Map<ApplicationUser>(userDto);
            if (!ModelState.IsValid)
            {
                return BadRequest("Data is not Correctes , Plz Enter Data is Valid");
            }
            try
            {
                UserStore<IdentityUser> userStore = new UserStore<IdentityUser>(new TaskDbContext());
                IdentityUser identity = new IdentityUser();
                identity.UserName = user.UserName;
                identity.PasswordHash = user.Password;
                IdentityResult result = await _userManager.CreateAsync(identity, user.Password);
                if (result.Succeeded)
                {
                    return Ok(new Response { Status="Error",Message="User Created Successfully"});
                }
                else
                {
                    return BadRequest(result.Errors.ToList()[0]);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> Login(LoginDto model)
        {
            var user = await _userManager.FindByNameAsync(model.Username);
            if (user != null && await _userManager.CheckPasswordAsync(user, model.Password))
            {
                var authClaims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, user.UserName),
                    new Claim(ClaimTypes.NameIdentifier,user.Id),
                    new Claim(ClaimTypes.Role,user.Id),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                };
                var token = new JwtSecurityToken(
                    issuer: _configuration["JWT:Issuer"],
                    audience: _configuration["JWT:Audience"],
                    expires: DateTime.Now.AddHours(3),
                    claims: authClaims
                    );
                return Ok(new
                {
                    token = new JwtSecurityTokenHandler().WriteToken(token),
                    expiration = token.ValidTo
                });
            }
            return Unauthorized();
        }

        [HttpPut]
        [Route("ResetPassword")]
        public async Task<IActionResult> Restpassword(ApplicationUserDTO model)
        {
            var user = await _userManager.FindByEmailAsync(model.UserName);
            if (!ModelState.IsValid)
            {
                return BadRequest("Data Not Valid");
            }
            try
            {
                user.PasswordHash = model.Password;
                IdentityResult result = await _userManager.UpdateAsync(user);
                if (result.Succeeded)
                {
                    _taskDbContext.SaveChanges();
                    return Ok();
                }
                else
                {
                    return BadRequest(result.Errors.ToList()[0]);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
