﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SoapWebService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiCore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SoapWebServiceController : ControllerBase
    {
        [HttpGet("AllRateQuote")]
        public System.Threading.Tasks.Task<SoapWebService.RateQuoteReply> GetRateQuoteAsync(string APIKey, SoapWebService.RateQuoteRequest request)
        {
            return GetRateQuoteAsync(APIKey, request);
        }

    }
}
