import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './ComponentsAuth/login/login.component';
import { RegisterComponent } from './ComponentsAuth/register/register.component';
import { AuthGuard } from './Shared/auth.guard';

const routes: Routes = [
  {path:"Register",component : RegisterComponent},
  {path:'login', component: LoginComponent ,canActivate:[AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
