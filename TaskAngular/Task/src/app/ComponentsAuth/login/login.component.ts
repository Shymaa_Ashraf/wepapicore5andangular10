import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/Services/api.service';
import { LoginUsers } from 'src/app/Shared/Login';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginuser=new LoginUsers('','','');
  isLoginError : boolean =false;
  constructor(private apiService:ApiService,private router: Router) { }

  ngOnInit() {}
  onSubmit(username:any,password:any){  
    this.apiService.userAuthintication(username,password).subscribe((data:any)=>{
      localStorage.setItem('userToken',data.access_token);     
      this.router.navigate(["/Register"]);
    },
    (err:HttpErrorResponse)=>{
      this.isLoginError =true;
    }
    );    
  }

}
