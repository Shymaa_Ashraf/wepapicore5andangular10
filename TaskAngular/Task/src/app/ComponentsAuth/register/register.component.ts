import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/Services/user.service';
import { Users } from 'src/app/Shared/User';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  userModel=new Users('','','','');
  constructor(private userservice:UserService,private router: Router)
   { }

  ngOnInit(): void {
    this.reserform();
  }
  reserform(form? : NgForm){
    if(form !=null)
      form.reset();
    this.userModel= {
      username : '',
      email:'',
      password : '',
      confirmpassword :''
    }
  }
  OnSubmit(form : NgForm){
    this.userservice.registerUser(this.userModel).subscribe((data:any)=>{
      if(data.Succeeded == true)
      this.reserform(form);
      // this.router.navigate(["/dashboard"]);
    });
  }
}
